#pragma once

#include <utility>

namespace tshare
{

struct no_specific_options{};

namespace detail
{

template <typename Default, typename Specific>
struct options_selector
{
    using has_specific_options = std::true_type;

    Default default_opts;
    Specific specific_opts;
};

template <typename Default>
struct options_selector<Default, no_specific_options>
{
    using has_specific_options = std::false_type;

    Default default_opts;
};

} // namespace detail

template <typename DefaultOptions, typename SpecificOptions = no_specific_options>
class server_options
{
    using opts_selector = detail::options_selector<DefaultOptions, SpecificOptions>;
public:
    [[nodiscard]]
    constexpr const DefaultOptions& get_default_options() const noexcept
    {
        return opts_.default_opts;
    }

    constexpr void set_default_options(DefaultOptions def_opts)
    {
        opts_.default_opts = std::move(def_opts);
    }

    [[nodiscard]]
    constexpr const SpecificOptions& get_specific_options() const noexcept
    {
        static_assert(
            std::is_same_v<typename opts_selector::has_specific_options, std::true_type>,
            "specific options are not available"
        );
        return opts_.specific_opts;
    }

    constexpr void set_specific_options(SpecificOptions spec_opts)
    {
        static_assert(
            std::is_same_v<typename opts_selector::has_specific_options, std::true_type>,
            "specific options are not available"
        );
        opts_.specific_opts = std::move(spec_opts);
    }

private:
    opts_selector opts_;
};

} // namespace tshare
