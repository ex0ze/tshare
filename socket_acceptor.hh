#pragma once

#include <boost/asio/any_io_executor.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <optional>

namespace tshare
{

struct super_class_executor_binder
{
    template <typename Server>
    decltype(auto) operator()(Server& server)
    {
        using super_class_type = typename Server::super_class_type;
        return static_cast<super_class_type&>(server)
            .make_executor_for_socket();
    }
};

template <
    typename Super,
    typename Protocol = boost::asio::ip::tcp,
    typename ExecutorBinder = super_class_executor_binder
>
class socket_acceptor
{
public:
    using super_class_type = Super;

    using acceptor_access = socket_acceptor;
    using binder_access = ExecutorBinder;

    using protocol_type = Protocol;
    using acceptor_type = typename protocol_type::acceptor;
    using endpoint_type = typename protocol_type::endpoint;
    using socket_type   = typename protocol_type::socket;

protected:
    explicit socket_acceptor(const boost::asio::any_io_executor& executor, const ExecutorBinder& binder = ExecutorBinder{})
    : acceptor_{executor}
    , exec_binder_{binder}
    {}

    void listen(const endpoint_type& listen_endpoint, protocol_type proto)
    {
        acceptor_.open(proto);
        acceptor_.set_option(boost::asio::socket_base::reuse_address(true));
        acceptor_.bind(listen_endpoint);
        acceptor_.listen();
    }

    void accept_next()
    {
        socket_.emplace(exec_binder_(*this));

        acceptor_.async_accept(*socket_, [this](auto error) {
            static_cast<super_class_type&>(*this)
                .on_socket_accepted(std::move(*socket_), error);
        });
    }

    [[nodiscard]] const acceptor_type& get_acceptor() const noexcept { return acceptor_; }
    [[nodiscard]] acceptor_type& get_acceptor() noexcept { return acceptor_; }
    [[nodiscard]] const ExecutorBinder& get_executor_binder() const noexcept { return exec_binder_; }
    [[nodiscard]] ExecutorBinder& get_executor_binder() noexcept { return exec_binder_; }

private:
    acceptor_type acceptor_;
    ExecutorBinder exec_binder_;
 
    std::optional<socket_type> socket_;
};

} // namespace tshare
