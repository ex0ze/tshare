#pragma once

#include <functional>

#include "server_options.hh"
#include "socket_acceptor.hh"
#include "types.hh"

namespace tshare
{

struct fetch_server_default_options
{
    static constexpr int64_t default_receive_timeout_ms = 10000; //10SEC
    static constexpr int64_t default_send_timeout_ms = 10000; //10SEC
    static constexpr uint64_t default_max_message_size = 65000; //65KB, enough for simple GET request 

    types::millisec receive_timeout_ms = types::millisec{default_receive_timeout_ms};
    types::millisec send_timeout_ms = types::millisec{default_send_timeout_ms};
    uint64_t max_message_size = default_max_message_size;
};

class fetch_server
: private socket_acceptor<fetch_server>
{
    friend socket_acceptor::acceptor_access;
    friend socket_acceptor::binder_access;

public:
    using endpoint = boost::asio::ip::tcp::endpoint;
    using error_code = boost::system::error_code;
    using options = server_options<fetch_server_default_options>;
    using fetch_message_fn = std::function<types::shared_message(std::string_view)>;

    explicit fetch_server(const boost::asio::any_io_executor& executor, options opts = options{});

    void start(const endpoint& listen_endpoint, fetch_message_fn fetch_message);
    
    [[nodiscard]] constexpr const options& get_options() const noexcept
    {
        return opts_;
    }

    [[nodiscard]] constexpr const fetch_message_fn& get_fetch_message_fn() const noexcept
    {
        return fetch_message_;
    }

private:
    // socket_acceptor interface
    [[nodiscard]] boost::asio::any_io_executor make_executor_for_socket() const;
    void on_socket_accepted(socket_type accepted_socket, error_code err);

    boost::asio::any_io_executor outer_executor_;

    options opts_;
    fetch_message_fn fetch_message_;
};

} // namespace tshare
