#include "timed_client_connection.hh"

#include <boost/asio.hpp>

#include <optional>

namespace io = boost::asio;
using boost::system::error_code;

namespace tshare
{

class timed_client_connection::impl
{
public:
    explicit impl(timed_client_connection& conn, timed_client_connection::socket client_socket) noexcept
    : conn_{conn}
    , socket_{std::move(client_socket)}
    {
    }

    ~impl()
    {
        // gracefully close connection
        do_close();
    }

    void set_read_timeout(types::millisec timeout)
    {
        if (!timer_)
            timer_.emplace(socket_.get_executor());
        
        read_timeout_ = timeout;
    }

    void set_write_timeout(types::millisec timeout)
    {
        if (!timer_)
            timer_.emplace(socket_.get_executor());
        
        write_timeout_ = timeout;
    }

    void set_max_read_size(size_t size)
    {
        max_read_size_ = size;
    }

    void set_read_chunk_size(size_t size)
    {
        read_chunk_size = size;
    }

    [[nodiscard]] io::any_io_executor get_executor() noexcept
    {
        return socket_.get_executor();
    }

    void async_recv_message()
    {
        do_read_next_chunk();
    }

    void async_send_message(io::const_buffer buf)
    {
        do_set_op_timeout<op_type::write>();
        do_start_async_write(buf);
    }

    void async_send_message(const types::buffer_sequence& buffers)
    {
        do_set_op_timeout<op_type::write>();
        do_start_async_write(buffers);
    }

private:
    enum class op_type
    {
        read,
        write
    };
    enum class timeout_state
    {
        idle,
        in_progress,
        timed_out,
        finished
    };

    void do_read_next_chunk()
    {
        do_set_op_timeout<op_type::read>();
        do_start_async_read();
    }

    void do_start_async_read()
    {
        auto buf = do_acquire_next_chunk_buffer();
        size_t expected = buf.size();

        socket_.async_read_some(
            buf,
            [this, self = acquire_connection(), expected](error_code err, size_t actual_bytes) {
                on_async_read_done(err, expected, actual_bytes);
            }
        );
    }

    void on_async_read_done(error_code ec, size_t expected_bytes, size_t actual_bytes)
    {
        assert(expected_bytes >= actual_bytes);
        do_correct_buffer_size(expected_bytes, actual_bytes);

        if (timeout_state_ == timeout_state::timed_out) {
            if (message_.empty())
                return conn_.on_empty_message();

            return conn_.on_message(std::move(message_));
        }

        if (ec) {
            if (ec == io::error::operation_aborted) return;
            return do_handle_error(ec, "async_read");
        }

        do_notify_timer<op_type::read>();

        if (max_read_size_ && *max_read_size_ == message_.size()) {
            return conn_.on_max_size_reached(std::move(message_));
        }

        do_read_next_chunk();
    }

    void do_start_async_write(io::const_buffer buffer)
    {
        io::async_write(
            socket_,
            buffer,
            [this, self = acquire_connection()](error_code ec, size_t) {
                on_async_write_done(ec);
            }
        );
    }

    void do_start_async_write(const types::buffer_sequence& buffers)
    {
        io::async_write(
            socket_,
            buffers,
            [this, self = acquire_connection()](error_code ec, size_t) {
                on_async_write_done(ec);
            }
        );
    }

    void on_async_write_done(error_code ec)
    {
        if (ec) return do_handle_error(ec, "async_write");
        
        do_notify_timer<op_type::write>();
        conn_.on_send_done();
    }

    [[nodiscard]] io::mutable_buffer do_acquire_next_chunk_buffer()
    {
        const size_t prev_size = message_.size();
        size_t to_read = 0;
        if (max_read_size_) {
            const size_t bytes_remaining = *max_read_size_ - message_.size();
            to_read = std::min(read_chunk_size, bytes_remaining);
        }
        else {
            to_read = read_chunk_size;
        }

        message_.resize(prev_size + to_read, boost::container::default_init);
        return io::mutable_buffer{
            std::next(message_.data(), static_cast<ptrdiff_t>(prev_size)),
            to_read
        };
    }

    void do_correct_buffer_size(size_t expected_bytes, size_t actual_bytes)
    {
        if (expected_bytes == actual_bytes) return;

        const auto diff = std::max(expected_bytes, actual_bytes) -
                          std::min(expected_bytes, actual_bytes);
        message_.resize(message_.size() - diff, boost::container::default_init);
    }

    template<op_type type>
    [[nodiscard]] bool has_timeout() const noexcept
    {
        if constexpr (type == op_type::read)
            return read_timeout_ != types::millisec::max();
        else
            return write_timeout_ != types::millisec::max();
    }

    template <op_type type>
    [[nodiscard]] types::millisec get_timeout() const noexcept
    {
        if constexpr (type == op_type::read)
            return read_timeout_;
        else
            return write_timeout_;
    }

    template <op_type type>
    void do_set_op_timeout()
    {
        if (!has_timeout<type>()) return;

        timer_->expires_from_now(get_timeout<type>());
        timer_->async_wait([this, self = acquire_connection()](error_code ec) {
            on_timer_done(ec);
        });

        timeout_state_ = timeout_state::in_progress;
    }

    template <op_type type>
    void do_notify_timer()
    {
        if (!has_timeout<type>()) return;

        timeout_state_ = timeout_state::finished;
        timer_->cancel();
    }

    [[nodiscard]] timed_client_connection::shared acquire_connection() const noexcept
    {
        return conn_.shared_this();
    }

    void on_timer_done(error_code ec)
    {
        if (timeout_state_ == timeout_state::finished) return;
        
        if (ec) {
            if (ec == io::error::operation_aborted) return;
            return do_handle_error(ec, "timer");
        }

        timeout_state_ = timeout_state::timed_out;
        socket_.cancel();
    }

    void do_handle_error(error_code ec, std::string_view context)
    {
        conn_.on_error(ec, context);
        do_close();
    }

    void do_close()
    {
        error_code ignore_ec;

        socket_.shutdown(io::socket_base::shutdown_both, ignore_ec);
        socket_.close(ignore_ec);

        if (timer_) {
            timeout_state_ = timeout_state::finished;
            timer_->cancel();
        }
    }

    timed_client_connection& conn_;
    timed_client_connection::socket socket_;

    std::optional<io::steady_timer> timer_;
    types::millisec read_timeout_ = types::millisec::max();
    types::millisec write_timeout_ = types::millisec::max();
    timeout_state timeout_state_ = timeout_state::idle;

    types::message message_;
    std::optional<size_t> max_read_size_;
    size_t read_chunk_size = 8192;
};

timed_client_connection::~timed_client_connection() = default;

io::any_io_executor timed_client_connection::get_executor() noexcept
{
    return impl_->get_executor();
}

void timed_client_connection::async_recv_message()
{
    impl_->async_recv_message();
}

void timed_client_connection::async_send_message(io::const_buffer buffer)
{
    impl_->async_send_message(buffer);
}

void timed_client_connection::async_send_message(const types::buffer_sequence& buffers)
{
    impl_->async_send_message(buffers);
}

void timed_client_connection::set_read_timeout(types::millisec timeout)
{
    impl_->set_read_timeout(timeout);
}

void timed_client_connection::set_write_timeout(types::millisec timeout)
{
    impl_->set_write_timeout(timeout);
}

void timed_client_connection::set_max_read_size(size_t max_read_size)
{
    impl_->set_max_read_size(max_read_size);
}

void timed_client_connection::set_read_chunk_size(size_t size) noexcept
{
    impl_->set_read_chunk_size(size);
}

timed_client_connection::timed_client_connection(socket client_socket)
: impl_{std::make_unique<impl>(*this, std::move(client_socket))}
{
}

} // namespace tshare
