#pragma once

#include <spdlog/spdlog.h>

namespace tshare
{

using logger_ptr = std::shared_ptr<spdlog::logger>;

namespace detail
{

void init_log();
void shutdown_log();

} // namespace detail

logger_ptr stdout_log();
logger_ptr stderr_log();

struct scoped_log_init
{
    explicit scoped_log_init()
    {
        detail::init_log();
    }
    ~scoped_log_init()
    {
        detail::shutdown_log();
    }
};

} // namespace tshare
