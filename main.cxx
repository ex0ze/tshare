#include <iostream>

#include <boost/asio.hpp>
#include <boost/beast.hpp>

#include "logger.hh"
#include "tshare_server.hh"

using boost::system::error_code;
namespace io = boost::asio;

int32_t main(int argc, char* argv[])
{
    boost::ignore_unused(argc, argv);

    try {
        tshare::scoped_log_init log_init;
        tshare::stdout_log()->debug("Logger initialized");

        io::io_context ctx;

        io::signal_set sset{ctx, SIGINT, SIGTERM};
        tshare::server server{ctx.get_executor()};

        sset.async_wait([&ctx](error_code ec, int sig) {
            if (ec && ec != io::error::operation_aborted)
                tshare::stderr_log()->error("signal set async wait: {}", ec.message());

            tshare::stdout_log()->info(
                "Caught {}, finishing...",
                sig == SIGINT ? "SIGINT" : "SIGTERM"
            );
            ctx.stop();
        });

        io::ip::tcp::endpoint fetch_server_listen_endpoint {
            io::ip::make_address("127.0.0.1"),
            80
        };
        io::ip::tcp::endpoint paste_server_listen_endpoint {
            io::ip::make_address("127.0.0.1"),
            5566
        };
        server.start(fetch_server_listen_endpoint, paste_server_listen_endpoint);
        ctx.run();
    }
    catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
