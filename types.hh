#pragma once

#include <fmt/format.h>

#include <boost/asio/buffer.hpp>
#include <boost/container/vector.hpp>

#include <array>
#include <chrono>
#include <memory>
#include <string_view>
#include <vector>

namespace tshare::types
{

struct size_bytes
{
    uint64_t value;
};

using message = boost::container::vector<char>;
using shared_message = std::shared_ptr<message>;
using buffer_sequence = std::vector<boost::asio::const_buffer>;

using sec = std::chrono::seconds;
using millisec = std::chrono::milliseconds;

} // namespace tshare::types

namespace fmt
{

template <>
struct formatter<tshare::types::size_bytes> : formatter<double>
{
    template <typename FormatContext>
    auto format(tshare::types::size_bytes sz_bytes, FormatContext& ctx)
    {
        using namespace std::string_view_literals;
        using size_pair = std::pair<uint64_t, std::string_view>;
        size_pair sizes[] {
            {1'000'000'000, "GB"sv},
            {1'000'000,     "MB"sv},
            {1'000,         "KB"sv},
            {1,             "B"sv}
        };

        for (auto [quantity, description] : sizes) {
            if (sz_bytes.value >= quantity) {
                auto out = formatter<double>::format(
                    static_cast<double>(sz_bytes.value) / static_cast<double>(quantity),
                    ctx
                );
                for (char c : description) *out++ = c;
                return out;
            }
        }

        auto out = formatter<double>::format(0.0, ctx);
        *out++ = 'B';
        return out;
    }
};

} // namespace fmt
