#pragma once

#include <mutex>
#include <shared_mutex>

namespace tshare
{

class shared_synchronized_resource
{
public:
    template <typename Fn>
    decltype(auto) synchronized_for_read(Fn fn) const
    {
        std::shared_lock lock{mutex_};
        return fn();
    }
    template <typename Fn>
    decltype(auto) synchronized_for_write(Fn fn) const
    {
        std::unique_lock lock{mutex_};
        return fn();
    }

private:
    mutable std::shared_mutex mutex_;
};

class unique_synchronized_resource
{
public:
    template <typename Fn>
    decltype(auto) synchronized(Fn fn) const
    {
        std::lock_guard lock{mutex_};
        return fn();
    }

private:
    mutable std::mutex mutex_;
};

} // namespace tshare
