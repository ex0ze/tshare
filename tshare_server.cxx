#include "tshare_server.hh"

#include "uuid_key_generator.hh"

namespace io = boost::asio;

namespace
{

thread_local tshare::uuid_key_generator key_generator;

} // anonymous namespace

namespace tshare
{

server::server(const io::any_io_executor& executor)
: fetch_server_{executor}
, paste_server_{executor}
{

}

void server::start(const endpoint& fetch_listen_endpoint, const endpoint& paste_listen_endpoint)
{
    fetch_server_.start(fetch_listen_endpoint, [this](auto req) {
        return on_fetch_server_request(req);
    });

    paste_server_.start(paste_listen_endpoint, [this](auto req) {
        return on_paste_server_request(std::move(req));
    });
}

types::shared_message server::on_fetch_server_request(std::string_view req)
{
    return storage_.get_message(req);
}

std::string server::on_paste_server_request(types::shared_message msg)
{
    auto expiry_time = std::chrono::system_clock::now() + std::chrono::days{1};
    auto key = key_generator.generate_uuid();
    storage_.put_message(key, std::move(msg), expiry_time);
    return key;
}

} // namespace tshare
