#pragma once

#include "fetch_server.hh"
#include "paste_server.hh"
#include "paste_storage.hh"

namespace tshare
{

class server
{
public:
    using endpoint = boost::asio::ip::tcp::endpoint;

    explicit server(const boost::asio::any_io_executor& executor);

    void start(const endpoint& fetch_listen_endpoint, const endpoint& paste_listen_endpoint);

private:
    types::shared_message on_fetch_server_request(std::string_view req);
    std::string on_paste_server_request(types::shared_message msg);

    fetch_server fetch_server_;
    paste_server paste_server_;
    timed_storage storage_;
};

} // namespace tshare

