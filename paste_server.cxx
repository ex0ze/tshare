#include "paste_server.hh"

#include <boost/asio.hpp>
#include <boost/core/ignore_unused.hpp>

#include <memory>

#include "logger.hh"
#include "timed_client_connection.hh"

namespace io = boost::asio;
using boost::system::error_code;
using boost::system::system_error;

namespace tshare
{

namespace
{

#define LOG_ARGS(FMT, ...) \
    "[session {}:{}] " FMT, \
    client_ep_.address().to_string(), \
    client_ep_.port(), \
    __VA_ARGS__

#define LOG_INFO(FMT, ...) \
    stdout_log()->info( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

#define LOG_ERROR(FMT, ...) \
    stderr_log()->error( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

#define LOG_DEBUG(FMT, ...) \
    stdout_log()->debug( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

class paste_server_session final
: public std::enable_shared_from_this<paste_server_session>
, public timed_client_connection
{
public:
    using shared = std::shared_ptr<paste_server_session>;

    static shared create(paste_server& server, timed_client_connection::socket socket)
    {
        auto client_ep = socket.remote_endpoint();
        stdout_log()->info(
            "Started paste server session {}:{} -> {}:{}",
            client_ep.address().to_string(),
            client_ep.port(),
            socket.local_endpoint().address().to_string(),
            socket.local_endpoint().port()
        );

        shared ptr { new paste_server_session{server, std::move(socket)} };
        ptr->set_client_endpoint(std::move(client_ep));

        return ptr;
    }

private:
    explicit paste_server_session(paste_server& server, timed_client_connection::socket socket) noexcept
    : timed_client_connection{std::move(socket)}
    , server_{server}
    {
    }

    void on_error(error_code ec, std::string_view context) override
    {
        LOG_ERROR("{}: {}", context, ec.message());
    }

    void on_message(types::message msg) override
    {
        LOG_DEBUG("Got message of size {}", msg.size());

        auto shared_msg = std::make_shared<types::message>(std::move(msg));

        response_message_ = server_.get_save_message_fn()(std::move(shared_msg));
        LOG_DEBUG("Got address for message: {}", response_message_);

        do_correct_message_newline();
        async_send_message(boost::asio::buffer(response_message_));
    }

    void on_max_size_reached(types::message msg) override
    {
        LOG_DEBUG("Got truncated message of size {}", types::size_bytes{msg.size()});
        const auto msg_size = msg.size();
        auto shared_msg = std::make_shared<types::message>(std::move(msg));

        response_message_ = server_.get_save_message_fn()(std::move(shared_msg));
        LOG_DEBUG("Got address for message: {}", response_message_);

        // use scatter gather io to prepend info about message truncation
        response_part_1_ = fmt::format("Too large message received, truncated to {}\n", types::size_bytes{msg_size});
        do_correct_message_newline();
        response_buffers_.push_back(io::buffer(response_part_1_));
        response_buffers_.push_back(io::buffer(response_message_));

        async_send_message(response_buffers_);
    }

    void on_empty_message() override
    {
        static std::string_view empty_message_error {"Empty message received\n"};
        LOG_DEBUG("{}", "Empty message received");
        async_send_message(boost::asio::buffer(empty_message_error));
    }

    void on_send_done() override
    {
        LOG_DEBUG("{}", "Session ended successfully");
    }

    void set_client_endpoint(io::ip::tcp::endpoint client_ep) noexcept
    {
        client_ep_ = std::move(client_ep);
    }

    timed_client_connection::shared shared_this() override
    {
        return shared_from_this();
    }

    void do_correct_message_newline()
    {
        if (!response_message_.empty() && response_message_.back() != '\n')
            response_message_.push_back('\n');
    }

    paste_server& server_;
    io::ip::tcp::endpoint client_ep_;
    std::string response_message_;

    types::buffer_sequence response_buffers_;
    std::string response_part_1_;
};

#undef LOG_INFO
#undef LOG_ERROR
#undef LOG_DEBUG

} // anonymous namespace

paste_server::paste_server(const boost::asio::any_io_executor& executor, options opts)
: socket_acceptor{boost::asio::make_strand(executor)}
, outer_executor_{executor}
, opts_{std::move(opts)}
{}

void paste_server::start(const endpoint& listen_endpoint, save_message_fn save_message)
{
    save_message_ = std::move(save_message);

    stdout_log()->info(
        "Starting Paste server on {}:{}",
        listen_endpoint.address().to_string(),
        listen_endpoint.port()
    );
    listen(listen_endpoint, io::ip::tcp::v4());

    accept_next();
}

//////socket_acceptor callbacks
boost::asio::any_io_executor paste_server::make_executor_for_socket() const
{
    return boost::asio::make_strand(outer_executor_);
}

void paste_server::on_socket_accepted(socket_type accepted_socket, error_code err)
{
    if (err && err != io::error::operation_aborted)
        throw system_error{err, "acceptor"};

    const auto& opts = get_options().get_default_options();
    auto session = paste_server_session::create(*this, std::move(accepted_socket));

    session->set_max_read_size(opts.max_message_size);
    session->set_read_timeout(opts.receive_timeout_ms);
    session->set_write_timeout(opts.send_timeout_ms);
    session->async_recv_message();

    accept_next();
}

} // namespace tshare
