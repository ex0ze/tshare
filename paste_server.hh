#pragma once

#include <boost/asio/any_io_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <chrono>

#include "server_options.hh"
#include "socket_acceptor.hh"
#include "types.hh"

namespace tshare
{

struct paste_server_default_options
{
    static constexpr int64_t default_receive_timeout_ms = 5000;
    static constexpr int64_t default_send_timeout_ms = 5000;
    static constexpr uint64_t default_max_message_size = 20'000'000; //20MB 

    types::millisec receive_timeout_ms = types::millisec{default_receive_timeout_ms};
    types::millisec send_timeout_ms = types::millisec{default_send_timeout_ms};
    uint64_t max_message_size = default_max_message_size;
};

class paste_server
: private socket_acceptor<paste_server>
{
    friend socket_acceptor::acceptor_access;
    friend socket_acceptor::binder_access;

public:
    using endpoint = boost::asio::ip::tcp::endpoint;
    using error_code = boost::system::error_code;
    using socket_type = socket_acceptor::socket_type;
    using options = server_options<paste_server_default_options>;
    using save_message_fn = std::function<std::string(types::shared_message)>;

    explicit paste_server(const boost::asio::any_io_executor& executor, options opts = options{});

    void start(const endpoint& listen_endpoint, save_message_fn save_message);

    [[nodiscard]] constexpr const options& get_options() const noexcept
    {
        return opts_;
    }

    [[nodiscard]] constexpr const save_message_fn& get_save_message_fn() const noexcept
    {
        return save_message_;
    }

private:
    // socket_acceptor interface
    [[nodiscard]] boost::asio::any_io_executor make_executor_for_socket() const;
    void on_socket_accepted(socket_type accepted_socket, error_code err);

    boost::asio::any_io_executor outer_executor_;

    options opts_;
    save_message_fn save_message_;
};

} // namespace tshare
