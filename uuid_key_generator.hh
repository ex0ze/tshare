#pragma once

#include <boost/uuid/uuid.hpp>

#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace tshare
{

class uuid_key_generator
{
public:
    std::string generate_uuid()
    {
        return boost::uuids::to_string(generator_());
    }
private:
    boost::uuids::random_generator generator_;
};

} // namespace tshare

