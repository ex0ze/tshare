#pragma once

#include <memory>

#include "synchronized_resource.hh"
#include "types.hh"

namespace tshare
{

class timed_storage : protected unique_synchronized_resource
{
public:
    using time_point_type = std::chrono::system_clock::time_point;

    explicit timed_storage();
    ~timed_storage();

    timed_storage(const timed_storage&) = delete;
    timed_storage(timed_storage&&) = default;
    timed_storage& operator=(const timed_storage&) = delete;
    timed_storage& operator=(timed_storage&&) = default;

    void put_message(std::string key, types::shared_message msg, time_point_type expiry_point);

    // nullptr if not found (not existed or expired)
    [[nodiscard]]
    types::shared_message get_message(std::string_view key);

private:
    class impl;
    std::unique_ptr<impl> impl_;
};

} // namespace tshare
