#include "logger.hh"

#include <spdlog/async.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace
{

std::string stderr_logger_name {"TShare stderr"};
std::string stdout_logger_name {"TShare stdout"};

} // anonymous namespace

namespace tshare
{

namespace detail
{

void init_log()
{
    using namespace std::string_literals;
    try {
        spdlog::create_async<spdlog::sinks::stderr_color_sink_mt>(stderr_logger_name)
            ->set_level(spdlog::level::debug);
        spdlog::create_async<spdlog::sinks::stdout_color_sink_mt>(stdout_logger_name)
            ->set_level(spdlog::level::debug);
    }
    catch (const std::exception& ex) {
        throw std::runtime_error{"Cannot init spdlog loggers: "s + ex.what()};
    }
    catch (...) {
        throw std::runtime_error{"Cannot init spdlog loggers: unknown error"};
    }
}

void shutdown_log()
{
    spdlog::shutdown();
}

} // namespace detail

logger_ptr stdout_log()
{
    return spdlog::get(stdout_logger_name);
}

logger_ptr stderr_log()
{
    return spdlog::get(stderr_logger_name);
}

} // namespace tshare
