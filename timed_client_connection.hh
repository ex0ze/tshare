#pragma once

#include <boost/asio/any_io_executor.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/system/error_code.hpp>

#include <functional>
#include <memory>
#include <string_view>

#include "types.hh"

namespace tshare
{

class timed_client_connection
{
public:
    using shared = std::shared_ptr<timed_client_connection>;
    using weak = std::weak_ptr<timed_client_connection>;
    using socket = boost::asio::ip::tcp::socket;

    virtual ~timed_client_connection();

    timed_client_connection(const timed_client_connection&) = delete;
    timed_client_connection& operator=(const timed_client_connection&) = delete;
    timed_client_connection(timed_client_connection&&) = default;
    timed_client_connection& operator=(timed_client_connection&&) = default;

    [[nodiscard]] boost::asio::any_io_executor get_executor() noexcept;

    void async_recv_message();
    void async_send_message(boost::asio::const_buffer buffer);
    // scatter gather interface
    void async_send_message(const types::buffer_sequence& buffers);

    void set_read_timeout(types::millisec timeout);
    void set_write_timeout(types::millisec timeout);
    void set_max_read_size(size_t max_read_size);
    void set_read_chunk_size(size_t size) noexcept;

    [[nodiscard]] virtual shared shared_this() = 0;

    virtual void on_error(boost::system::error_code ec, std::string_view context) = 0;
    virtual void on_message(types::message msg) = 0;
    virtual void on_max_size_reached(types::message msg) = 0;
    virtual void on_empty_message() = 0;
    virtual void on_send_done() = 0;

protected:
    explicit timed_client_connection(socket client_socket);

private:
    class impl;
    std::unique_ptr<impl> impl_;
};

} // namespace tshare
