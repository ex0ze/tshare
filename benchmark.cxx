#include <benchmark/benchmark.h>

#include <boost/beast.hpp>

#include <sstream>

namespace http = boost::beast::http;

static auto make_response()
{
    http::response<http::empty_body> resp;

    resp.version(11);
    resp.set(http::field::server, "TShare 1.0 server");
    resp.set(http::field::content_type, "text/html");
    resp.keep_alive(false);
    resp.result(http::status::bad_request);
    resp.prepare_payload();

    return resp;
}

template <typename Resp>
static std::string make_comparable(const Resp& r)
{
    std::ostringstream ss;
    ss << r;
    return std::move(ss).str();
}

static void BM_DefaultOstringStream(benchmark::State& state)
{
    auto resp = make_response();
    auto cmp = make_comparable(resp);

    for (auto _ : state) {
        static thread_local std::ostringstream oss;
        oss.str(std::string{});
        oss << resp;
        std::string result = std::move(oss).str();
        if (result != cmp) std::abort();
    }
}

BENCHMARK(BM_DefaultOstringStream);

BENCHMARK_MAIN();
