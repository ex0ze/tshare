#include "fetch_server.hh"

#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/callable_traits.hpp>

#include <optional>
#include <sstream>

#include "logger.hh"

namespace io = boost::asio;
namespace http = boost::beast::http;
using boost::system::error_code;
using boost::system::system_error;

namespace tshare
{

namespace
{

#define LOG_ARGS(FMT, ...) \
    "[session {}:{}] " FMT, \
    client_ep_.address().to_string(), \
    client_ep_.port(), \
    __VA_ARGS__

#define LOG_INFO(FMT, ...) \
    stdout_log()->info( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

#define LOG_ERROR(FMT, ...) \
    stderr_log()->error( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )
#define LOG_WARNING(FMT, ...) \
    stderr_log()->warn( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

#define LOG_DEBUG(FMT, ...) \
    stdout_log()->debug( \
        LOG_ARGS(FMT, __VA_ARGS__) \
    )

template <typename T>
std::string serialize_to_string(const T& value)
{
    static thread_local std::ostringstream stream;
    stream.str(std::string{});
    stream.clear();

    stream << value;

    return std::move(stream).str();
}

class fetch_server_session
: public std::enable_shared_from_this<fetch_server_session>
{
public:
    using shared = std::shared_ptr<fetch_server_session>;

    static shared create(fetch_server& server, io::ip::tcp::socket socket)
    {
        auto client_ep = socket.remote_endpoint();
        stdout_log()->info(
            "Started fetch server session {}:{} -> {}:{}",
            client_ep.address().to_string(),
            client_ep.port(),
            socket.local_endpoint().address().to_string(),
            socket.local_endpoint().port()
        );

        shared ptr { new fetch_server_session{server, std::move(socket)} };
        ptr->set_client_endpoint(std::move(client_ep));

        return ptr;
    }

    void start()
    {
        do_async_read_request();
    }

private:
    explicit fetch_server_session(fetch_server& server, io::ip::tcp::socket socket)
    : server_{server}
    , stream_{std::move(socket)}
    {
    }

    const auto& opts() const noexcept
    {
        return server_.get_options().get_default_options();
    }

    void do_async_read_request()
    {
        stream_.expires_after(opts().receive_timeout_ms);
        http::async_read_header(
            stream_,
            buffer_,
            req_parser_,
            [self = shared_from_this()](error_code ec, size_t bytes) {
                self->on_async_read_request_done(ec, bytes);
            }
        );
    }

    void on_async_read_request_done(error_code ec, size_t bytes)
    {
        boost::ignore_unused(bytes);
        if (ec) {
            if (ec == http::error::end_of_stream) 
                return do_close();
            if (ec == boost::beast::error::timeout) {
                return do_async_write_response([](http::response<http::string_body>& resp) {
                    resp.result(http::status::request_timeout);
                    resp.set(http::field::content_type, "text/plain");
                    resp.body() = "Request timed out";
                });
            }

            return on_error(ec, "async read request");
        }

        do_process_request();
    }

    void do_process_request()
    {
        auto& request = req_parser_.get();

        auto send_error_response = [&](std::string message) {
            LOG_DEBUG("{}", message);
            do_async_write_response([&](http::response<http::string_body>& resp) {
                resp.result(http::status::bad_request);
                resp.set(http::field::content_type, "text/plain");
                resp.body() = std::move(message);
            });
        };

        if (request.method() != http::verb::get) {
            return send_error_response(
                fmt::format(
                    "Wrong request method: expected {}, got {}",
                    static_cast<std::string>(http::to_string(http::verb::get)),
                    static_cast<std::string>(http::to_string(request.method()))
                )
            );
        }

        auto target = request.target();
        if (!target.empty() && target.front() == '/')
            target.remove_prefix(1);
        if (target.empty())
            return send_error_response("Empty target is not allowed");

        auto target_sv = std::string_view{target.data(), target.size()};
        paste_ = server_.get_fetch_message_fn()(target_sv);

        if (!paste_) {
            LOG_DEBUG("Requested paste {}, expired or does not exist", target_sv);

            return do_async_write_response([&](http::response<http::string_body>& resp) {
                resp.result(http::status::not_found);
                resp.set(http::field::content_type, "text/plain");
                resp.body() = "Paste expired or does not exist";
            });
        }

        LOG_DEBUG("Requested paste {}, size={}", target_sv, paste_->size());

        do_async_write_response([&](http::response<http::buffer_body>& resp) {
            resp.result(http::status::ok);
            resp.set(http::field::content_type, "text/plain");
            resp.body().data = paste_->data();
            resp.body().size = paste_->size();
            resp.body().more = false;
        });
    }

    void do_close()
    {
        error_code ec;
        stream_.socket().shutdown(io::socket_base::shutdown_both, ec);
        if (ec) on_error(ec, "close");
    }

    template <typename ResponseSetter>
    void do_async_write_response(ResponseSetter setter)
    {
        using setter_args = boost::callable_traits::args_t<std::decay_t<ResponseSetter>>;
        using response_type = std::tuple_element_t<0, setter_args>;
        using response_type_decayed = std::decay_t<response_type>;
        static_assert(std::is_reference_v<response_type>, "Response must be taken by reference");

        auto response = std::make_shared<response_type_decayed>();
        response->version(11);
        response->set(http::field::server, "TShare 1.0 HTTP Server");
        response->keep_alive(false);

        setter(*response);

        response->prepare_payload();
        stream_.expires_after(opts().send_timeout_ms);
        http::async_write(
            stream_,
            *response,
            [self = shared_from_this(), response](error_code ec, size_t bytes) {
                self->on_async_write_response_done(ec, bytes);
            }
        );
    }

    void on_async_write_response_done(error_code ec, size_t bytes)
    {
        boost::ignore_unused(bytes);
        if (ec) return on_error(ec, "async write response");

        do_close();
    }

    void on_error(error_code ec, std::string_view context)
    {
        LOG_ERROR("{}: {}", context, ec.message());
    }

    void set_client_endpoint(io::ip::tcp::endpoint client_ep) noexcept
    {
        client_ep_ = std::move(client_ep);
    }

    using request_parser = http::request_parser<http::empty_body>; 

    fetch_server& server_;
    boost::beast::tcp_stream stream_;
    boost::beast::flat_buffer buffer_;
    request_parser req_parser_;
    io::ip::tcp::endpoint client_ep_;

    types::shared_message paste_;
};

} // anonymous namespace

fetch_server::fetch_server(const boost::asio::any_io_executor& executor, options opts)
: socket_acceptor{boost::asio::make_strand(executor)}
, outer_executor_{executor}
, opts_{std::move(opts)}
{}

void fetch_server::start(const endpoint& listen_endpoint, fetch_message_fn fetch_message)
{
    fetch_message_ = std::move(fetch_message);

    stdout_log()->info(
        "Starting Fetch server on {}:{}",
        listen_endpoint.address().to_string(),
        listen_endpoint.port()
    );

    listen(listen_endpoint, io::ip::tcp::v4());
    accept_next();
}

boost::asio::any_io_executor fetch_server::make_executor_for_socket() const
{
    return boost::asio::make_strand(outer_executor_);
}

void fetch_server::on_socket_accepted(socket_type accepted_socket, error_code err)
{
    if (err && err != io::error::operation_aborted)
        throw system_error{err, "acceptor"};

    const auto& opts = get_options().get_default_options();
    fetch_server_session::create(*this, std::move(accepted_socket))->start();

    accept_next();
}

} // namespace tshare
