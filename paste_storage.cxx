#include "paste_storage.hh"

#include <boost/multi_index_container.hpp>

#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/key.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/tag.hpp>

#include "logger.hh"

namespace mi = boost::multi_index;

namespace tshare
{

struct paste_record
{
    std::string key;
    types::shared_message message;
    timed_storage::time_point_type expiry_point;

    struct by_key{};
    struct by_expiry_point{};
};

class timed_storage::impl
{
public:
    void put_record(paste_record record)
    {
        remove_expired_records();

        records_.insert(std::move(record));
    }

    types::shared_message get_record(std::string_view key)
    {
        remove_expired_records();

        const auto& key_index = records_.get<paste_record::by_key>();
        auto iter = key_index.find(
            key,
            std::hash<std::string_view>{},
            [](const auto& a, const auto& b) { return a == b; }
        );
        if (iter != key_index.end())
            return iter->message;
        return nullptr;
    }

private:
    void remove_expired_records()
    {
        auto& expiry_index = records_.get<paste_record::by_expiry_point>();
        auto now = std::chrono::system_clock::now();

        if (expiry_index.empty() || expiry_index.begin()->expiry_point > now)
            return;
        
        auto begin = expiry_index.begin();
        auto end = expiry_index.upper_bound(now);
        while (begin != end) {
            stdout_log()->debug("[timed storage] paste {} has expired", begin->key);
            expiry_index.erase(begin++);
        }
    }

    using container_type = mi::multi_index_container<
        paste_record,
        mi::indexed_by<
            mi::ordered_non_unique<
                mi::tag<paste_record::by_expiry_point>,
                mi::key<&paste_record::expiry_point>
            >,
            mi::hashed_unique<
                mi::tag<paste_record::by_key>,
                mi::key<&paste_record::key>,
                std::hash<std::string>
            >
        >
    >;

    container_type records_;

};


timed_storage::timed_storage()
: impl_{std::make_unique<impl>()}
{
}

timed_storage::~timed_storage() = default;

void timed_storage::put_message(std::string key, types::shared_message msg, time_point_type expiry_point)
{
    synchronized([&]() mutable {
        impl_->put_record(paste_record{std::move(key), std::move(msg), expiry_point});
    });
}

types::shared_message timed_storage::get_message(std::string_view key)
{
    return synchronized([&] {
        return impl_->get_record(key);
    });
}

} // namespace tshare
